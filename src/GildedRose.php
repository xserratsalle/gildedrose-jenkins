<?php

require_once("ItemFactory.php");

class GildedRose {
	/**
	 * Función que actualiza la calidad y el SellIn de un conjunto de items.
	 */
	public static function updateQuality(array $items){
		foreach($items as $currentItem){
			$itemFactory = ItemFactory::makeItem($currentItem);
			$itemFactory->decreaseSellIn();
			$itemFactory->updateQuality();
		}
	}
}
