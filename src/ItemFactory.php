<?php

require_once("ItemFactory.php");
require_once("CheeseItem.php");
require_once("ConcertItem.php");
require_once("ConjuredItem.php");
require_once("DefaultItem.php");
require_once("MythicalItem.php");

class ItemFactory {
    const CHEESE_TYPE_ITEM = "Aged Brie";
    const CONCERT_TYPE_ITEM = "Backstage passes to a TAFKAL80ETC concert";
    const MYTHICAL_TYPE_ITEM = "Sulfuras, Hand of Ragnaros";
    const CONJURED_TYPE_ITEM = "Conjured Mana Cake";

    public static function makeItem(Item $item){
        $itemName = $item->getName();
        if($itemName === self::CHEESE_TYPE_ITEM){
            return new CheeseItem($item);
        } elseif($itemName === self::CONCERT_TYPE_ITEM){
            return new ConcertItem($item);
        } elseif($itemName === self::CONJURED_TYPE_ITEM){
            return new ConjuredItem($item);
        } elseif($itemName === self::MYTHICAL_TYPE_ITEM){
            return new MythicalItem($item);
        } else{
            return new DefaultItem($item);
        }
    }
} 