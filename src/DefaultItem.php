<?php

require_once("ItemDecorator.php");
require_once("Item.php");

final class DefaultItem extends ItemDecorator{

    public function updateQuality(){
        if($this->hasReachedMinimumSellInDays()){
            $this->decreaseQuality(2);
        } else{
            $this->decreaseQuality(1);
        }
    }
}