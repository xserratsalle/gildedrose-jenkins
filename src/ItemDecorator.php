<?php

require_once("Item.php");

abstract class ItemDecorator extends Item{

    const MINIMUM_SELL_IN_DAYS = 0;
    const SELL_IN_GRANULARITY = 1;
    const MAX_QUALITY = 50;
    const MIN_QUALITY = 0;

    const QUALITY_GRANULARITY = 1;

    protected  $item;

    public function __construct(Item $item){
        $this->item = $item;
    }

    public function getName(){
        return $this->item->getName();
    }

    public function setName($name){
        $this->item->setName($name);
    }

    public function getQuality(){
        return $this->item->getQuality();
    }

    public function setQuality($quality){
        $this->item->setQuality($quality);
    }

    public function getSellIn(){
        return $this->item->getSellIn();
    }

    public function setSellIn($sellIn){
        $this->item->setSellIn($sellIn);
    }

    protected function increaseQuality($value){
        if($this->getQuality() < self::MAX_QUALITY){
            $this->setQuality($this->getQuality() + $value);
        }
    }
    protected function decreaseQuality($value){
        if($this->getQuality() > self::MIN_QUALITY){
            $this->setQuality($this->getQuality() - $value);
        }
    }

    public function decreaseSellIn(){
        $this->setSellIn($this->getSellIn() - self::SELL_IN_GRANULARITY);
    }

    protected function hasReachedMinimumSellInDays(){
        return $this->getSellIn() < self::MINIMUM_SELL_IN_DAYS;
    }

    abstract public function updateQuality();
}