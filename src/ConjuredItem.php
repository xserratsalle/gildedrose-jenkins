<?php

require_once("ItemDecorator.php");

final class ConjuredItem extends ItemDecorator{

    public function updateQuality(){
        if($this->hasReachedMinimumSellInDays()){
            $this->decreaseQuality(4);
        } else{
            $this->decreaseQuality(2);
        }
    }
}