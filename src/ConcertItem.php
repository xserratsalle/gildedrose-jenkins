<?php

require_once("ItemDecorator.php");

final class ConcertItem extends ItemDecorator{
    const CONCERT_SELL_IN_DOUBLE_THERSHOLD = 10;
    const CONCERT_SELL_IN_TRIPLE_THERSHOLD = 5;

    public function updateQuality(){
        if($this->getSellIn() > self::CONCERT_SELL_IN_DOUBLE_THERSHOLD){
            $this->increaseQuality(1);

        } elseif($this->getSellIn() <= self::CONCERT_SELL_IN_DOUBLE_THERSHOLD && $this->getSellIn() > self::CONCERT_SELL_IN_TRIPLE_THERSHOLD){
            $this->increaseQuality(2);

        } elseif($this->getSellIn() <= self::CONCERT_SELL_IN_TRIPLE_THERSHOLD && $this->getSellIn() > self::MINIMUM_SELL_IN_DAYS){
            $this->increaseQuality(3);

        } else{
            $this->setQuality(self::MIN_QUALITY);
        }
    }
}