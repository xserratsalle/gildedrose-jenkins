<?php

require_once("ItemDecorator.php");

final class CheeseItem extends ItemDecorator{
    public function updateQuality(){
        if ($this->hasReachedMinimumSellInDays()) {
            $this->increaseQuality(2);
        } else{
            $this->increaseQuality(1);
        }
    }
}